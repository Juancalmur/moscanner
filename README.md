## Solution used

For the development of this application, I used Bluetooth adapters and broadcast receivers to manage the connection between the scanner and the nearby devices. At first, the application scans for nearby devices ready to connect. After it finishes searching for the first time, the user can tap the 'SCAN' button to search again in case the user wants to register a new device not already scanned. If this button is pressed again its caption is changed to 'STOP' and if the user presses this button again it will stop the exploration of newer devices. Below this button, it's displayed the different devices found in a list, with some basic information about the device and a 'CONNECT' button at it's right. If this button is pressed, it will show a new screen, using a new activity, with some of the information the scanner recovered about this particular device.

---

## Challenges

- Solved

1. The application scans nearby, ready to connect, Bluetooth devices, and manages to display them on screen with some basic information like the name of the device and MAC address.
2. The application also scans ready to connect Bluetooth devices at the press of the button 'SCAN' and it promptly changes its caption to 'STOP', making it possible to end the scant started by the user, changing its caption to 'SCAN' again.
3. The application manages to display another screen at the press of the button 'CONNECT' with the same basic information gathered in the scan of the device selected.
4. The application follows the suggested design and works around it.

- Unsolved

1. The UI of the application is not organized with Fragments.
2. The application does not use a dependency injector.
3. The application does not have the processing of Bluetooth in a separate library.
4. The application does not obtain all the specified data required in the test, although it does obtain some data that proves that the scanner actually scans nearby devices.

---

## My experience

Although this is my first time using Android, and of course my first time programming something close to an app for scanning Bluetooth devices, I really enjoyed programming this test. I learned a lot about mobile development and in the process, I realized that I like it quite a bunch. I would like to thank you for giving me the opportunity to carry out this test because, as I already stated, not only did I learned a lot about something new and interesting to me, but also I found something I think is fun developing.