package com.example.moscanner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DeviceInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_info);

        // Display of the device's name on the view
        String name = getIntent().getStringExtra("NAME_DEVICE");
        TextView nameDevice = findViewById(R.id.localNameText);
        nameDevice.setText(name);

        // Display of the device's address on the view
        String address = getIntent().getStringExtra("ADDRESS_DEVICE");
        TextView addressDevice = findViewById(R.id.manufacturerDataText);
        addressDevice.setText(address);

        // Display of the device's uuid on the view
        String uuid = getIntent().getStringExtra("UUID_DEVICE");
        TextView uuidDevice = findViewById(R.id.uuidText);
        uuidDevice.setText(uuid);
    }
}
