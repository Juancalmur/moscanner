package com.example.moscanner;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class DeviceListAdapter extends ArrayAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<BluetoothDevice> devices;
    private int viewResourceId;

    // Constructor in charge of assembling the device for the devices list in MainActivity
    public DeviceListAdapter(Context context, int tvResourceId, ArrayList<BluetoothDevice> devices){
        super(context, tvResourceId, devices);
        this.devices = devices;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewResourceId = tvResourceId;
    }

    // Method in charge of getting the information out of the device so it can be displayed in the view
    // that is going to be inserted in the devices list
    public View getView(int position, View convertView, ViewGroup parent){

        convertView = layoutInflater.inflate(viewResourceId, null);
        BluetoothDevice device = devices.get(position);

        if (device != null){

            // Fetch of textView
            TextView deviceName = convertView.findViewById(R.id.tvDeviceName);
            TextView deviceAddress = convertView.findViewById(R.id.tvDeviceAddress);

            // Fetch of information of the device
            String name = device.getName();
            String address = device.getAddress();

            // Set name
            if (name != null){
                deviceName.setText(name);
            }
            else{
                deviceName.setText("No name available");
            }

            // Set address
            if (address != null){
                deviceAddress.setText(address);
            }
            else{
                deviceAddress.setText("No address available");
            }

        }

        return convertView;

    }

}
