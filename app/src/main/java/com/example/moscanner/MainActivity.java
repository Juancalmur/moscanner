package com.example.moscanner;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    BluetoothAdapter btAdapter;
    public ArrayList<BluetoothDevice> btDevices = new ArrayList<>();
    public DeviceListAdapter deviceListAdapter;
    ListView lv;

    boolean scanning;

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private int mConnectionState = STATE_DISCONNECTED;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    // Method in charge of receiving information about the status
    // of the Bluetooth connection of the device
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(btAdapter.ACTION_STATE_CHANGED)){
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, btAdapter.ERROR);

                switch(state){
                    case BluetoothAdapter.STATE_OFF:
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        break;
                    case BluetoothAdapter.STATE_ON:
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }
            }
        }
    };

    // Method in charge of receiving information about the Bluetooth devices
    // found in the periphery of the scanner and in charge of calling the view
    // who displays them as a list
    private BroadcastReceiver broadcastReceiver2 = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
        @Override
        public void onReceive(Context context, Intent intent) {

            final String action = intent.getAction();

            if (action.equals(BluetoothDevice.ACTION_FOUND)){

                // Fetch of the Bluetooth device
                final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                btDevices.add(device);

                // Intent of connecting via Gatt
                /*String uuid = "";
                BluetoothGatt gatt = device.connectGatt(context, false, gattCallback);
                for (BluetoothGattService gattService : gatt.getServices()) {
                    Log.i("TAG", "Service UUID Found: " + gattService.getUuid().toString());
                }*/

                // Call to deviceListAdapter class and add device to the list
                deviceListAdapter = new DeviceListAdapter(context, R.layout.device_adapter_view, btDevices);
                lv.setAdapter(deviceListAdapter);

            }
        }
    };

    // Method triggered at the end of the application
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    // Method triggered at the start of the application
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = findViewById(R.id.connectableList);
        btDevices = new ArrayList<>();
        scanning = false;
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        enableBT();
        discover();

        Toast myToast = Toast.makeText(getApplicationContext(), "Finished setup", Toast.LENGTH_LONG);
        myToast.show();

    }

    // Method in charge of turning on Bluetooth on your device if it's not enabled
    // using an intent part of the BluetoothAdapter library
    public void enableBT(){

        if(!btAdapter.isEnabled()){

            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBtIntent);

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(broadcastReceiver, BTIntent);

        }

    }

    // Method in charge of turning off Bluetooth on your device if it's enabled
    // using an intent part of the BluetoothAdapter library
    public void disableBT(){

        if(btAdapter.isEnabled()){

            btAdapter.disable();

            // Intent of reaching bluetooth broadcast
            IntentFilter BtIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(broadcastReceiver, BtIntent);

        }

    }

    // Method in charge of triggering the start of the discovery of nearby
    // Bluetooth devices around the scanner
    public void discover() {

        checkBtPermissions();

        stopDiscover();

        // Start of the scan and intent of reaching device broadcast
        btAdapter.startDiscovery();
        IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(broadcastReceiver2, discoverDevicesIntent);

    }

    // Method in charge of stopping the discovery of nearby
    // Bluetooth devices around the scanner
    public void stopDiscover(){

        btAdapter.cancelDiscovery();

    }

    // Method in charge of checking the permissions necessary for the
    // enabling of the Bluetooth in the device
    public void checkBtPermissions(){

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){

            int permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");

            if (permissionCheck != 0){
                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
            }

        }

    }

    // Method that reacts to the tap of the button "SCAN" which activates
    // the method that starts the discover of nearby Bluetooth devices
    public void scanButtonTap(View v) {

        Button scanButton = findViewById(R.id.scanButton);

        if(!scanning) {

            scanButton.setText("Stop");
            clearList();
            discover();
            Toast myToast = Toast.makeText(getApplicationContext(), "Scanning", Toast.LENGTH_LONG);
            myToast.show();
            scanning = true;

        }

        else if(scanning){

            scanButton.setText("Scan");
            stopDiscover();
            Toast myToast = Toast.makeText(getApplicationContext(), "Stopped scanning", Toast.LENGTH_LONG);
            myToast.show();
            scanning = false;

        }

    }

    // Method that reacts to the tap of the button "CONNECT" which triggers
    // the second activity in charge of displaying extra information about
    // this particular device
    public void connectButtonTap(View v){

        View parent = (View)v.getParent();

        if (parent != null) {

            TextView deviceName = parent.findViewById(R.id.tvDeviceName);
            String nameDevice = deviceName.getText().toString();

            TextView deviceAddress = parent.findViewById(R.id.tvDeviceAddress);
            String addressDevice = deviceAddress.getText().toString();

            TextView deviceUUID = parent.findViewById(R.id.tvDeviceUUID);
            String uuidDevice = deviceUUID.getText().toString();

            Intent intent = new Intent(this, DeviceInfoActivity.class);
            intent.putExtra("NAME_DEVICE", nameDevice);
            intent.putExtra("ADDRESS_DEVICE", addressDevice);
            intent.putExtra("UUID_DEVICE", uuidDevice);
            startActivity(intent);

        }

    }

    // Method that clears the list that displays the diferent Bluetooth devices
    public void clearList(){
        btDevices.clear();
        lv.setAdapter(null);
    }

    // Method in charge of managing a Gatt connection between the Bluetooth scanner and the
    // device selected
    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                mConnectionState = STATE_CONNECTED;
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                mConnectionState = STATE_DISCONNECTED;
            }
        }
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
            } else {
            }
        }
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
            }
        }
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
        }
    };

}
